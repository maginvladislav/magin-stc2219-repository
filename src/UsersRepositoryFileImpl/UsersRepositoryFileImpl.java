package UsersRepositoryFileImpl;
import java.util.*;

public class UsersRepositoryFileImpl
{
    static List<User> users = Reader.getAllUsers();
    static HashMap <Integer, User> userHashMap = new HashMap<>();
    static HashSet<Integer> idHashSet = new HashSet<>();
    public static void main(String[] args)  {
        for (User user : users) {
            userHashMap.put(user.getId(), user);
            idHashSet.add(user.getId());
        }

        findById(5);
        delete(5);
        update(findById(3));
        create(new User(5, "Alladin", "Rakshasov", 19, false));
        create(new User(1, "Alladin", "Rakshasov", 19, false));


    }
    public static User findById (int id){
        if (userHashMap.get(id) == null){
            throw new NullPointerException("Пользователя с таким ID не существует (Поиск)");
        }
        return userHashMap.get(id);
    }
    public static void update (User user){
        user.setName("Nicolas");
        user.setAge(50);
//        user.setName("Kolyan");
//        user.setAge(27);
        Writer.writeUsersInFile(users);
    }
    public static void create (User user) {
        if (idHashSet.contains(user.getId())) {
            throw new IllegalArgumentException("С предоставленным ID " + user.getId() + ", пользователь уже существует.");
        }
        users.add(user);
        Writer.writeUsersInFile(users);
        System.out.println("Создан пользователь с ID " + user.getId());
    }
    public static void delete (int id){
        if (userHashMap.get(id) == null){
            throw new NullPointerException("Пользователя с таким ID не существует (Удаление)");
        }
        users.remove(users.indexOf(findById(id)));
        userHashMap.remove(id);
        idHashSet.remove(id);
        Writer.writeUsersInFile(users);
        System.out.println("Удален пользователь с ID " + id);
    }
}

