package UsersRepositoryFileImpl;

public class User {
    private int id;
    private String name;
    private String lastname;
    private int age;
    private boolean job;

    public User (int id, String name, String lastname, int age, boolean job) {
        this.id = id;
        this.name = name;
        this.lastname = lastname;
        this.age = age;
        this.job = job;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return id + "|" + name + "|" + lastname + "|" + age + "|" + job + "\n";
    }
}


