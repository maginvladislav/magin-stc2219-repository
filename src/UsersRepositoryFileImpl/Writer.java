package UsersRepositoryFileImpl;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

public class Writer {
    public static void writeUsersInFile (List<User> users) {
        try (FileWriter writer = new FileWriter("TextFile.txt")) {
            for (User user : users) {
                writer.write(user.toString());
            }
        } catch (IOException exception) {
            throw new RuntimeException();
        }
    }
}
