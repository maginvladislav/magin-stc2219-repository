package UsersRepositoryFileImpl;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class Reader {
    public static ArrayList<User> getAllUsers () {
        ArrayList<User> users = new ArrayList<>();

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("TextFile.txt"))) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                String [] infoUser = line.split("\\|");
                int id = Integer.parseInt(infoUser[0]);
                String name = infoUser[1];
                String lastname = infoUser[2];
                int age = Integer.parseInt(infoUser[3]);
                boolean job = Boolean.parseBoolean(infoUser[4]);
                users.add(new User(id, name, lastname, age, job));
            }
        } catch (IOException e) {
            throw new RuntimeException();
        }
        return users;
    }
}
